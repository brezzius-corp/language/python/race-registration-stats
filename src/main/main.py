#!/usr/bin/python3

"""Get Betrail stats from Njuko registration"""

import http.client as chttp
import urllib.parse as uparse
import os
import sys
import configparser
import json
import unidecode

from bs4 import BeautifulSoup


def progress(current, total):
    """Show progress bar"""

    bar = ['-'] * 60
    cbar = current * len(bar) // total
    cper = current * 100 // total

    print('[', end='')

    for i in range(cbar):
        print('#', end='')

    for i in range(cbar, len(bar)):
        print('-', end='')

    print("] {}%".format(cper), end='\r', flush=True)


def stats(config, lastname, firstname):
    """Get stats from Betrail for specified name"""

    params = uparse.urlencode({'queryString': lastname + " " + firstname})
    query = (config['betrail']['pathname']) + "?" + params

    cnx = chttp.HTTPSConnection(config['betrail']['hostname'])
    cnx.request("GET", query)

    response = cnx.getresponse()

    blastname = " ".join(unidecode.unidecode(lastname.upper().replace('-', ' ').replace('\'', ' ')).split())
    bfirstname = " ".join(unidecode.unidecode(firstname.upper().replace('-', ' ').replace('\'', ' ')).split())
    name = blastname + " " + bfirstname

    level = 0

    if response.status == 200 and response.reason == 'OK':
        data = json.loads(response.read())

        for runner in data['body'][0]['close_runners']:
            if runner['_source']['title'] == name:
                level = runner['_source']['bt_level']

                if level != 0:
                    break

    cnx.close()

    return level


def parse(config, data):
    """Parse data"""

    result = {}

    html = BeautifulSoup(data, "html.parser")

    last = int(html.find("span", {"class": "last-item-number"}).text)
    table = html.find("table", {"id": "registrations-table"}).find("tbody")

    i = 1

    print("Calcul de l'index des {} coureurs ...\n".format(last))

    for item in table.find_all("tr"):
        line = item.find_all("td")

        lastname = line[0].text
        firstname = line[1].text

        level = stats(config, lastname, firstname)
        result[lastname + " " + firstname] = level / 100

        progress(i, last)

        i+=1

    print(end='\n\n')

    for key, value in sorted(result.items(), key=lambda x: x[1], reverse=True):
        print(f"{key} : {value}")


def main():
    """Main function"""

    if len(sys.argv) != 3:
        print("Program needs event and race identifiant")
        sys.exit(1)

    print("\033[?25l", end='')

    config = configparser.ConfigParser()
    config.read(os.path.dirname(sys.argv[0]) + '/config.ini')

    name = (config['njuko']['pathname']).format(sys.argv[1], sys.argv[2])

    cnx = chttp.HTTPSConnection(config['njuko']['hostname'])
    cnx.request("GET", name)

    response = cnx.getresponse()
    data = response.read()

    if response.status == 200 and response.reason == 'OK':
        parse(config, data)

    cnx.close()

    print("\033[?25h", end='')


if __name__ == "__main__":
    main()
